package com.mkdev.sheypoortestproject.service;

import com.mkdev.sheypoortestproject.arch.models.detail.DetailModel;
import com.mkdev.sheypoortestproject.arch.models.main.MainListModel;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApplicationApi {

    @GET("listings")
    Observable<MainListModel> getListOfItems(
            @Query("count") int count,
            @Query("offset") int offset,
            @Query("sortPresetID") String sortPresetID,
            @Query("withImage") String withImage
    );

    @GET("listings/{listingID}")
    Observable<DetailModel> getDetail(@Path("listingID") String id);
}