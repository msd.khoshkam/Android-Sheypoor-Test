package com.mkdev.sheypoortestproject.service;

import android.util.Log;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import retrofit2.HttpException;

public abstract class NetworkCallback<M> implements Subscriber<M> {

    private static final String TAG = NetworkCallback.class.getName();

    public abstract void onSuccess(M model);

    public abstract void onFailure(String message);

    public abstract void onFinish();

    @Override
    public void onSubscribe(Subscription s) {

    }

    @Override
    public void onComplete() {
        onFinish();
    }

    @Override
    public void onNext(M model) {
        onSuccess(model);
    }

    @Override
    public void onError(Throwable e) {
        e.printStackTrace();
        if (e instanceof HttpException) {
            HttpException httpException = (HttpException) e;
            int code = httpException.code();
            String message = httpException.getMessage();
            Log.i(TAG, "code : " + code);
            onFailure(message);

        } else {
            onFailure(e.getMessage());
        }
        onFinish();
    }
}
