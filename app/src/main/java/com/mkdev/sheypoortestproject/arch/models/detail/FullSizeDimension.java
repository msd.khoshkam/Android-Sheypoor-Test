package com.mkdev.sheypoortestproject.arch.models.detail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FullSizeDimension implements Parcelable {

    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("height")
    @Expose
    private Integer height;
    public final static Creator<FullSizeDimension> CREATOR = new Creator<FullSizeDimension>() {


        @SuppressWarnings({
                "unchecked"
        })
        public FullSizeDimension createFromParcel(Parcel in) {
            return new FullSizeDimension(in);
        }

        public FullSizeDimension[] newArray(int size) {
            return (new FullSizeDimension[size]);
        }

    };

    protected FullSizeDimension(Parcel in) {
        this.width = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.height = ((Integer) in.readValue((Integer.class.getClassLoader())));
    }

    public FullSizeDimension() {
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(width);
        dest.writeValue(height);
    }

    public int describeContents() {
        return 0;
    }

}
