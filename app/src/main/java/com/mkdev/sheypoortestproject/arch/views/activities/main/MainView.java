package com.mkdev.sheypoortestproject.arch.views.activities.main;

import android.content.Intent;

import com.mkdev.sheypoortestproject.arch.models.main.MainListModel;

public interface MainView {

    void showLoading();

    void hideLoading();

    void showMoreDataLoading();

    void hideMoreDataLoading();

    void getDataSuccess(MainListModel model);

    void getMoreDataSuccess(MainListModel model);

    void getDataFail(String message);

    void moveToDetail(Intent intent);
}
