package com.mkdev.sheypoortestproject.arch.models.main;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MainListModel implements Parcelable {

    @SerializedName("totalCount")
    @Expose
    private Integer totalCount;
    @SerializedName("requestDateTime")
    @Expose
    private String requestDateTime;
    @SerializedName("lastReceivedValue")
    @Expose
    private Object lastReceivedValue;
    @SerializedName("serpMixItems")
    @Expose
    private Object serpMixItems;
    @SerializedName("listings")
    @Expose
    private List<Listing> listings = null;
    public final static Creator<MainListModel> CREATOR = new Creator<MainListModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public MainListModel createFromParcel(Parcel in) {
            return new MainListModel(in);
        }

        public MainListModel[] newArray(int size) {
            return (new MainListModel[size]);
        }

    };

    protected MainListModel(Parcel in) {
        this.totalCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.requestDateTime = ((String) in.readValue((String.class.getClassLoader())));
        this.lastReceivedValue = ((Object) in.readValue((Object.class.getClassLoader())));
        this.serpMixItems = ((Object) in.readValue((Object.class.getClassLoader())));
        in.readList(this.listings, (Listing.class.getClassLoader()));
    }

    public MainListModel() {
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public String getRequestDateTime() {
        return requestDateTime;
    }

    public void setRequestDateTime(String requestDateTime) {
        this.requestDateTime = requestDateTime;
    }

    public Object getLastReceivedValue() {
        return lastReceivedValue;
    }

    public void setLastReceivedValue(Object lastReceivedValue) {
        this.lastReceivedValue = lastReceivedValue;
    }

    public Object getSerpMixItems() {
        return serpMixItems;
    }

    public void setSerpMixItems(Object serpMixItems) {
        this.serpMixItems = serpMixItems;
    }

    public List<Listing> getListings() {
        return listings;
    }

    public void setListings(List<Listing> listings) {
        this.listings = listings;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(totalCount);
        dest.writeValue(requestDateTime);
        dest.writeValue(lastReceivedValue);
        dest.writeValue(serpMixItems);
        dest.writeList(listings);
    }

    public int describeContents() {
        return 0;
    }
}
