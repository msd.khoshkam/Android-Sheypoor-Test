package com.mkdev.sheypoortestproject.arch.models.main;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Listing implements Parcelable {

    @SerializedName("listingID")
    @Expose
    private String listingID;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("thumbImageURL")
    @Expose
    private String thumbImageURL;
    @SerializedName("locationName")
    @Expose
    private String locationName;
    @SerializedName("attributes")
    @Expose
    private List<Attribute> attributes = null;
    @SerializedName("lastModifiedDate")
    @Expose
    private String lastModifiedDate;
    @SerializedName("separatorFlag")
    @Expose
    private Boolean separatorFlag;
    @SerializedName("separatorMessage")
    @Expose
    private String separatorMessage;
    public final static Creator<Listing> CREATOR = new Creator<Listing>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Listing createFromParcel(Parcel in) {
            return new Listing(in);
        }

        public Listing[] newArray(int size) {
            return (new Listing[size]);
        }

    };

    protected Listing(Parcel in) {
        this.listingID = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.thumbImageURL = ((String) in.readValue((String.class.getClassLoader())));
        this.locationName = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.attributes, (Attribute.class.getClassLoader()));
        this.lastModifiedDate = ((String) in.readValue((String.class.getClassLoader())));
        this.separatorFlag = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.separatorMessage = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Listing() {
    }

    public String getListingID() {
        return listingID;
    }

    public void setListingID(String listingID) {
        this.listingID = listingID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbImageURL() {
        return thumbImageURL;
    }

    public void setThumbImageURL(String thumbImageURL) {
        this.thumbImageURL = thumbImageURL;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public Boolean getSeparatorFlag() {
        return separatorFlag;
    }

    public void setSeparatorFlag(Boolean separatorFlag) {
        this.separatorFlag = separatorFlag;
    }

    public String getSeparatorMessage() {
        return separatorMessage;
    }

    public void setSeparatorMessage(String separatorMessage) {
        this.separatorMessage = separatorMessage;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(listingID);
        dest.writeValue(title);
        dest.writeValue(thumbImageURL);
        dest.writeValue(locationName);
        dest.writeList(attributes);
        dest.writeValue(lastModifiedDate);
        dest.writeValue(separatorFlag);
        dest.writeValue(separatorMessage);
    }

    public int describeContents() {
        return 0;
    }
}
