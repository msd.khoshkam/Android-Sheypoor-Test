package com.mkdev.sheypoortestproject.arch.models.detail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RelatedListing implements Parcelable {

    @SerializedName("listingID")
    @Expose
    private String listingID;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("thumbImageURL")
    @Expose
    private String thumbImageURL;
    @SerializedName("locationName")
    @Expose
    private String locationName;
    @SerializedName("attributes")
    @Expose
    private List<Attribute_> attributes = null;
    @SerializedName("lastModifiedDate")
    @Expose
    private String lastModifiedDate;
    public final static Creator<RelatedListing> CREATOR = new Creator<RelatedListing>() {


        @SuppressWarnings({
                "unchecked"
        })
        public RelatedListing createFromParcel(Parcel in) {
            return new RelatedListing(in);
        }

        public RelatedListing[] newArray(int size) {
            return (new RelatedListing[size]);
        }

    };

    protected RelatedListing(Parcel in) {
        this.listingID = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.thumbImageURL = ((String) in.readValue((String.class.getClassLoader())));
        this.locationName = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.attributes, (Attribute_.class.getClassLoader()));
        this.lastModifiedDate = ((String) in.readValue((String.class.getClassLoader())));
    }

    public RelatedListing() {
    }

    public String getListingID() {
        return listingID;
    }

    public void setListingID(String listingID) {
        this.listingID = listingID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbImageURL() {
        return thumbImageURL;
    }

    public void setThumbImageURL(String thumbImageURL) {
        this.thumbImageURL = thumbImageURL;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public List<Attribute_> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute_> attributes) {
        this.attributes = attributes;
    }

    public String getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(String lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(listingID);
        dest.writeValue(title);
        dest.writeValue(thumbImageURL);
        dest.writeValue(locationName);
        dest.writeList(attributes);
        dest.writeValue(lastModifiedDate);
    }

    public int describeContents() {
        return 0;
    }

}
