package com.mkdev.sheypoortestproject.arch.models.detail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Image implements Parcelable {

    @SerializedName("serverKey")
    @Expose
    private String serverKey;
    @SerializedName("isDefault")
    @Expose
    private Boolean isDefault;
    @SerializedName("fullSizeURL")
    @Expose
    private String fullSizeURL;
    @SerializedName("fullSizeDimension")
    @Expose
    private FullSizeDimension fullSizeDimension;
    @SerializedName("thumbnailURL")
    @Expose
    private String thumbnailURL;
    public final static Creator<Image> CREATOR = new Creator<Image>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        public Image[] newArray(int size) {
            return (new Image[size]);
        }

    };

    protected Image(Parcel in) {
        this.serverKey = ((String) in.readValue((String.class.getClassLoader())));
        this.isDefault = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.fullSizeURL = ((String) in.readValue((String.class.getClassLoader())));
        this.fullSizeDimension = ((FullSizeDimension) in.readValue((FullSizeDimension.class.getClassLoader())));
        this.thumbnailURL = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Image() {
    }

    public String getServerKey() {
        return serverKey;
    }

    public void setServerKey(String serverKey) {
        this.serverKey = serverKey;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public String getFullSizeURL() {
        return fullSizeURL;
    }

    public void setFullSizeURL(String fullSizeURL) {
        this.fullSizeURL = fullSizeURL;
    }

    public FullSizeDimension getFullSizeDimension() {
        return fullSizeDimension;
    }

    public void setFullSizeDimension(FullSizeDimension fullSizeDimension) {
        this.fullSizeDimension = fullSizeDimension;
    }

    public String getThumbnailURL() {
        return thumbnailURL;
    }

    public void setThumbnailURL(String thumbnailURL) {
        this.thumbnailURL = thumbnailURL;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(serverKey);
        dest.writeValue(isDefault);
        dest.writeValue(fullSizeURL);
        dest.writeValue(fullSizeDimension);
        dest.writeValue(thumbnailURL);
    }

    public int describeContents() {
        return 0;
    }
}
