package com.mkdev.sheypoortestproject.arch.views.activities.detail;

import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mkdev.mkdevlibrary.GlideApp;
import com.mkdev.mkdevlibrary.utils.Check;
import com.mkdev.mkdevlibrary.utils.ConvertDigits;
import com.mkdev.mkdevlibrary.utils.CustomToast;
import com.mkdev.mkdevlibrary.utils.UIUtil;
import com.mkdev.sheypoortestproject.R;
import com.mkdev.sheypoortestproject.arch.models.detail.DetailModel;
import com.mkdev.sheypoortestproject.arch.presenters.DetailPresenter;
import com.mkdev.sheypoortestproject.base.mvp.MvpActivity;

import butterknife.BindView;

public class DetailActivity extends MvpActivity<DetailPresenter>
        implements DetailView, View.OnClickListener {

    @BindView(R.id.imgThumbnail)
    ImageView imgThumbnail;
    @BindView(R.id.collapsingToolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appBarLayout)
    AppBarLayout appBarLayout;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvPrice)
    TextView tvPrice;
    @BindView(R.id.tvDescription)
    TextView tvDescription;
    @BindView(R.id.cvDetail)
    CardView cvDetail;
    @BindView(R.id.fabLike)
    FloatingActionButton fabLike;

    private String detailId;

    @Override
    protected DetailPresenter createPresenter() {
        return new DetailPresenter(this);
    }

    @Override
    protected void initBeforeView() {
        if (getIntent().hasExtra("id"))
            detailId = getIntent().getStringExtra("id");
    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_detail;
    }

    @Override
    protected void initViews() {
        initToolbar();
        setupBack(true, getString(R.string.app_name_fa));
        collapsingToolbar.setExpandedTitleColor(UIUtil.getColor(mContext, android.R.color.transparent));
        fabLike.setOnClickListener(this);
        presenter.loadData(detailId);
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
        cvDetail.setVisibility(View.GONE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
        cvDetail.setVisibility(View.VISIBLE);
    }

    @Override
    public void getDataSuccess(DetailModel item) {
        try {
            if (!Check.isEmpty(item.getTitle()))
                tvTitle.setText(item.getTitle());
            else
                tvTitle.setVisibility(View.GONE);

            if (item.getAttributes().size() > 0) {
                if (item.getAttributes().get(0).getAttributeValue().equals("توافقی"))
                    tvPrice.setText(item.getAttributes().get(0).getAttributeValue());
                else
                    tvPrice.setText(ConvertDigits.persianDigits(String.format(mContext.getString(R.string.price),
                            item.getAttributes().get(0).getAttributeValue())));
            } else
                tvPrice.setVisibility(View.GONE);

            if (!Check.isEmpty(item.getDescription()))
                tvDescription.setText(Html.fromHtml(item.getDescription()));
            else
                tvDescription.setVisibility(View.GONE);

            if (item.getImages().size() > 0) {
                if (!Check.isEmpty(item.getImages().get(0).getFullSizeURL())) {
                    GlideApp.with(mContext)
                            .load(item.getImages().get(0).getFullSizeURL())
                            .centerCrop()
                            .placeholder(R.mipmap.img_not_found_h1)
                            .error(R.mipmap.img_not_found_h1)
                            .into(imgThumbnail);
                } else
                    imgThumbnail.setImageDrawable(UIUtil.getDrawable(mContext, R.mipmap.img_not_found_h1));
            }
            collapsingToolbar.setTitle(item.getTitle());

        } catch (NullPointerException | IndexOutOfBoundsException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getDataFail(String message) {
        CustomToast.makeText(mContext, message, CustomToast.ERROR);
        finish();
    }

    @Override
    public void refreshData() {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabLike:

                break;
        }
    }
}
