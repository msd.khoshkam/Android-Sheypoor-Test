package com.mkdev.sheypoortestproject.arch.models.detail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailModel implements Parcelable {

    @SerializedName("listingID")
    @Expose
    private String listingID;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("categoryID")
    @Expose
    private Integer categoryID;
    @SerializedName("ownerID")
    @Expose
    private Integer ownerID;
    @SerializedName("images")
    @Expose
    private List<Image> images = null;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("attributes")
    @Expose
    private List<Attribute> attributes = null;
    @SerializedName("lastModifiedDateString")
    @Expose
    private String lastModifiedDateString;
    @SerializedName("listingURL")
    @Expose
    private String listingURL;
    @SerializedName("userType")
    @Expose
    private Integer userType;
    @SerializedName("moderationStatus")
    @Expose
    private Integer moderationStatus;
    @SerializedName("locationID")
    @Expose
    private Integer locationID;
    @SerializedName("locationType")
    @Expose
    private Integer locationType;
    @SerializedName("expirationDate")
    @Expose
    private String expirationDate;
    @SerializedName("phoneNumberIsVerified")
    @Expose
    private Boolean phoneNumberIsVerified;
    @SerializedName("hiddenPhoneNumber")
    @Expose
    private String hiddenPhoneNumber;
    @SerializedName("shouldShowContact")
    @Expose
    private Boolean shouldShowContact;
    @SerializedName("isChatEnabled")
    @Expose
    private Boolean isChatEnabled;
    @SerializedName("relatedListings")
    @Expose
    private List<RelatedListing> relatedListings = null;
    public final static Creator<DetailModel> CREATOR = new Creator<DetailModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DetailModel createFromParcel(Parcel in) {
            return new DetailModel(in);
        }

        public DetailModel[] newArray(int size) {
            return (new DetailModel[size]);
        }

    };

    protected DetailModel(Parcel in) {
        this.listingID = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.categoryID = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.ownerID = ((Integer) in.readValue((Integer.class.getClassLoader())));
        in.readList(this.images, (Image.class.getClassLoader()));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.attributes, (Attribute.class.getClassLoader()));
        this.lastModifiedDateString = ((String) in.readValue((String.class.getClassLoader())));
        this.listingURL = ((String) in.readValue((String.class.getClassLoader())));
        this.userType = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.moderationStatus = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.locationID = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.locationType = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.expirationDate = ((String) in.readValue((String.class.getClassLoader())));
        this.phoneNumberIsVerified = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.hiddenPhoneNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.shouldShowContact = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.isChatEnabled = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        in.readList(this.relatedListings, (RelatedListing.class.getClassLoader()));
    }

    public DetailModel() {
    }

    public String getListingID() {
        return listingID;
    }

    public void setListingID(String listingID) {
        this.listingID = listingID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Integer categoryID) {
        this.categoryID = categoryID;
    }

    public Integer getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(Integer ownerID) {
        this.ownerID = ownerID;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    public String getLastModifiedDateString() {
        return lastModifiedDateString;
    }

    public void setLastModifiedDateString(String lastModifiedDateString) {
        this.lastModifiedDateString = lastModifiedDateString;
    }

    public String getListingURL() {
        return listingURL;
    }

    public void setListingURL(String listingURL) {
        this.listingURL = listingURL;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public Integer getModerationStatus() {
        return moderationStatus;
    }

    public void setModerationStatus(Integer moderationStatus) {
        this.moderationStatus = moderationStatus;
    }

    public Integer getLocationID() {
        return locationID;
    }

    public void setLocationID(Integer locationID) {
        this.locationID = locationID;
    }

    public Integer getLocationType() {
        return locationType;
    }

    public void setLocationType(Integer locationType) {
        this.locationType = locationType;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Boolean getPhoneNumberIsVerified() {
        return phoneNumberIsVerified;
    }

    public void setPhoneNumberIsVerified(Boolean phoneNumberIsVerified) {
        this.phoneNumberIsVerified = phoneNumberIsVerified;
    }

    public String getHiddenPhoneNumber() {
        return hiddenPhoneNumber;
    }

    public void setHiddenPhoneNumber(String hiddenPhoneNumber) {
        this.hiddenPhoneNumber = hiddenPhoneNumber;
    }

    public Boolean getShouldShowContact() {
        return shouldShowContact;
    }

    public void setShouldShowContact(Boolean shouldShowContact) {
        this.shouldShowContact = shouldShowContact;
    }

    public Boolean getIsChatEnabled() {
        return isChatEnabled;
    }

    public void setIsChatEnabled(Boolean isChatEnabled) {
        this.isChatEnabled = isChatEnabled;
    }

    public List<RelatedListing> getRelatedListings() {
        return relatedListings;
    }

    public void setRelatedListings(List<RelatedListing> relatedListings) {
        this.relatedListings = relatedListings;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(listingID);
        dest.writeValue(title);
        dest.writeValue(categoryID);
        dest.writeValue(ownerID);
        dest.writeList(images);
        dest.writeValue(description);
        dest.writeList(attributes);
        dest.writeValue(lastModifiedDateString);
        dest.writeValue(listingURL);
        dest.writeValue(userType);
        dest.writeValue(moderationStatus);
        dest.writeValue(locationID);
        dest.writeValue(locationType);
        dest.writeValue(expirationDate);
        dest.writeValue(phoneNumberIsVerified);
        dest.writeValue(hiddenPhoneNumber);
        dest.writeValue(shouldShowContact);
        dest.writeValue(isChatEnabled);
        dest.writeList(relatedListings);
    }

    public int describeContents() {
        return 0;
    }

}
