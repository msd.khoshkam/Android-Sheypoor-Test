package com.mkdev.sheypoortestproject.arch.models.main;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attribute implements Parcelable {

    @SerializedName("attributeID")
    @Expose
    private Integer attributeID;
    @SerializedName("attributeValue")
    @Expose
    private String attributeValue;
    public final static Creator<Attribute> CREATOR = new Creator<Attribute>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Attribute createFromParcel(Parcel in) {
            return new Attribute(in);
        }

        public Attribute[] newArray(int size) {
            return (new Attribute[size]);
        }

    };

    protected Attribute(Parcel in) {
        this.attributeID = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.attributeValue = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Attribute() {
    }

    public Integer getAttributeID() {
        return attributeID;
    }

    public void setAttributeID(Integer attributeID) {
        this.attributeID = attributeID;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(attributeID);
        dest.writeValue(attributeValue);
    }

    public int describeContents() {
        return 0;
    }

}
