package com.mkdev.sheypoortestproject.arch.models.detail;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attribute_ implements Parcelable {

    @SerializedName("attributeID")
    @Expose
    private Integer attributeID;
    @SerializedName("attributeTitle")
    @Expose
    private String attributeTitle;
    @SerializedName("attributeValue")
    @Expose
    private String attributeValue;
    @SerializedName("attributeOrder")
    @Expose
    private Integer attributeOrder;
    @SerializedName("attributeIndex")
    @Expose
    private Integer attributeIndex;
    @SerializedName("shouldFillRow")
    @Expose
    private Boolean shouldFillRow;
    public final static Creator<Attribute_> CREATOR = new Creator<Attribute_>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Attribute_ createFromParcel(Parcel in) {
            return new Attribute_(in);
        }

        public Attribute_[] newArray(int size) {
            return (new Attribute_[size]);
        }

    };

    protected Attribute_(Parcel in) {
        this.attributeID = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.attributeTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.attributeValue = ((String) in.readValue((String.class.getClassLoader())));
        this.attributeOrder = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.attributeIndex = ((Integer) in.readValue((Integer.class.getClassLoader())));
        this.shouldFillRow = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
    }

    public Attribute_() {
    }

    public Integer getAttributeID() {
        return attributeID;
    }

    public void setAttributeID(Integer attributeID) {
        this.attributeID = attributeID;
    }

    public String getAttributeTitle() {
        return attributeTitle;
    }

    public void setAttributeTitle(String attributeTitle) {
        this.attributeTitle = attributeTitle;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public Integer getAttributeOrder() {
        return attributeOrder;
    }

    public void setAttributeOrder(Integer attributeOrder) {
        this.attributeOrder = attributeOrder;
    }

    public Integer getAttributeIndex() {
        return attributeIndex;
    }

    public void setAttributeIndex(Integer attributeIndex) {
        this.attributeIndex = attributeIndex;
    }

    public Boolean getShouldFillRow() {
        return shouldFillRow;
    }

    public void setShouldFillRow(Boolean shouldFillRow) {
        this.shouldFillRow = shouldFillRow;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(attributeID);
        dest.writeValue(attributeTitle);
        dest.writeValue(attributeValue);
        dest.writeValue(attributeOrder);
        dest.writeValue(attributeIndex);
        dest.writeValue(shouldFillRow);
    }

    public int describeContents() {
        return 0;
    }

}
