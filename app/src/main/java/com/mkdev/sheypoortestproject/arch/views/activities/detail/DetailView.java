package com.mkdev.sheypoortestproject.arch.views.activities.detail;

import com.mkdev.sheypoortestproject.arch.models.detail.DetailModel;

public interface DetailView {

    void showLoading();

    void hideLoading();

    void getDataSuccess(DetailModel model);

    void getDataFail(String message);

    void refreshData();
}
