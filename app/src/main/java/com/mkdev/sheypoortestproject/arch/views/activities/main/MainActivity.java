package com.mkdev.sheypoortestproject.arch.views.activities.main;

import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.MaterialDialog;
import com.mkdev.mkdevlibrary.customControll.RecyclerItemClickListener;
import com.mkdev.mkdevlibrary.utils.CustomToast;
import com.mkdev.mkdevlibrary.utils.DoubleClickExit;
import com.mkdev.mkdevlibrary.utils.TypeFaceUtils;
import com.mkdev.sheypoortestproject.R;
import com.mkdev.sheypoortestproject.adapters.MainAdapter;
import com.mkdev.sheypoortestproject.arch.models.main.Listing;
import com.mkdev.sheypoortestproject.arch.models.main.MainListModel;
import com.mkdev.sheypoortestproject.arch.presenters.MainPresenter;
import com.mkdev.sheypoortestproject.base.mvp.MvpActivity;

import java.util.List;

import butterknife.BindView;

public class MainActivity extends MvpActivity<MainPresenter>
        implements MainView {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.progress)
    ProgressBar progressBar;
    @BindView(R.id.progressLoadMore)
    ProgressBar progressLoadMore;

    private List<Listing> mList;
    private String sortPresetID = "n";
    private String withImage = null;
    private int offset = 0;
    private int count = 10;
    private boolean isUpdate = false;
    private MainAdapter mainAdapter;

    private boolean loading = true;
    int pastVisibleItems, visibleItemCount, totalItemCount;


    @Override
    protected MainPresenter createPresenter() {
        return new MainPresenter(this);
    }

    @Override
    protected void initBeforeView() {

    }

    @Override
    protected int getContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    protected void initViews() {
        initToolbar();
        setupBack(false, getString(R.string.app_name_fa));

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.addOnItemTouchListener(selectItemOnRecyclerView());
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0) //check for scroll down
                {
                    visibleItemCount = linearLayoutManager.getChildCount();
                    totalItemCount = linearLayoutManager.getItemCount();
                    pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();

                    if (loading) {
                        if ((visibleItemCount + pastVisibleItems) >= totalItemCount) {
                            loading = false;
                            offset = offset + count;
                            presenter.loadMoreData(count, offset + 1, sortPresetID, null);
                        }
                    }
                }
            }
        });

        presenter.loadData(count, offset, sortPresetID, null);
    }

    private RecyclerItemClickListener selectItemOnRecyclerView() {
        return new RecyclerItemClickListener(this, recyclerView,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        presenter.getItem(mList.get(position), mContext);
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // no set action
                    }
                });
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }


    @Override
    public void showMoreDataLoading() {
        progressLoadMore.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideMoreDataLoading() {
        progressLoadMore.setVisibility(View.GONE);
    }

    @Override
    public void getDataSuccess(MainListModel list) {
        mList = list.getListings();
        if (!isUpdate) {
            mainAdapter = new MainAdapter(mList);
            recyclerView.setAdapter(mainAdapter);
        } else {
            if (mainAdapter != null) {
                isUpdate = false;
                mainAdapter.update(mList);
                recyclerView.smoothScrollToPosition(0);
            }
        }
    }

    @Override
    public void getMoreDataSuccess(MainListModel list) {
        mainAdapter.addMoreData(list.getListings());
        loading = true;
    }

    @Override
    public void getDataFail(String message) {
        CustomToast.makeText(mContext, message, CustomToast.ERROR);
    }

    @Override
    public void moveToDetail(Intent intent) {
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_sort:
                showSortDialog();
                return true;

            case R.id.action_filter:
                showFilterDialog();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showSortDialog() {
        new MaterialDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.Theme_MatrialDialog))
                .title(R.string.sort)
                .typeface(TypeFaceUtils.getFontName(this), TypeFaceUtils.getFontName(this))
                .items(R.array.sortItems)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if (which == 0)
                            sortPresetID = "2"; //2 = Price ASC
                        else
                            sortPresetID = "3"; //3 = Price DESC
                        isUpdate = true;
                        presenter.loadData(count, offset, sortPresetID, null);
                        return true;
                    }
                })
                .show();
    }

    private void showFilterDialog() {
        new MaterialDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.Theme_MatrialDialog))
                .title(R.string.filter)
                .typeface(TypeFaceUtils.getFontName(this), TypeFaceUtils.getFontName(this))
                .items(R.array.filterItems)
                .itemsCallbackSingleChoice(-1, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        if (which == 0)
                            withImage = "yes"; // Whit Image only
                        else
                            withImage = null; // All record

                        isUpdate = true;
                        presenter.loadData(count, offset, sortPresetID, withImage);
                        return true;
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        if (!DoubleClickExit.check()) {
            CustomToast.makeText(this, getString(R.string.back_again_to_exit), CustomToast.INFO);
        } else {
            super.onBackPressed();
        }
    }
}
