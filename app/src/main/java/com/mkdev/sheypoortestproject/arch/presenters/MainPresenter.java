package com.mkdev.sheypoortestproject.arch.presenters;

import android.content.Context;
import android.content.Intent;

import com.mkdev.mkdevlibrary.LibraryApplication;
import com.mkdev.mkdevlibrary.network.ApiClient;
import com.mkdev.mkdevlibrary.network.RetrofitErrorHelper;
import com.mkdev.mkdevlibrary.utils.NetworkUtil;
import com.mkdev.sheypoortestproject.R;
import com.mkdev.sheypoortestproject.service.ApplicationApi;
import com.mkdev.sheypoortestproject.arch.models.main.Listing;
import com.mkdev.sheypoortestproject.arch.models.main.MainListModel;
import com.mkdev.sheypoortestproject.arch.views.activities.detail.DetailActivity;
import com.mkdev.sheypoortestproject.arch.views.activities.main.MainView;
import com.mkdev.sheypoortestproject.base.ui.BasePresenter;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter extends BasePresenter<MainView> {

    public MainPresenter(MainView view) {
        super.attachView(view);
    }

    public void loadData(int count, int offset, String sortPresetID, String withImage) {

        if (NetworkUtil.isNetworkConnected(LibraryApplication.getContext())) {
            view.showLoading();

            ApplicationApi mService = ApiClient.getInstance().getService(ApplicationApi.class);
            Observable<MainListModel> mObservable = mService.getListOfItems(count, offset, sortPresetID, withImage);

            mObservable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<MainListModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(MainListModel response) {
                            view.getDataSuccess(response);
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = (String) new RetrofitErrorHelper().getMessage(e);
                            view.getDataFail(message);
                            view.hideLoading();
                        }

                        @Override
                        public void onComplete() {
                            view.hideLoading();
                        }
                    });
        } else
            view.getDataFail(LibraryApplication.getContext().getString(R.string.no_connect_to_internet));

    }

    public void loadMoreData(int count, int offset, String sortPresetID, String withImage) {

        if (NetworkUtil.isNetworkConnected(LibraryApplication.getContext())) {
            view.showMoreDataLoading();

            ApplicationApi mService = ApiClient.getInstance().getService(ApplicationApi.class);
            Observable<MainListModel> mObservable = mService.getListOfItems(count, offset, sortPresetID, withImage);

            mObservable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<MainListModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(MainListModel response) {
                            view.getMoreDataSuccess(response);
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = (String) new RetrofitErrorHelper().getMessage(e);
                            view.getDataFail(message);
                            view.hideMoreDataLoading();
                        }

                        @Override
                        public void onComplete() {
                            view.hideMoreDataLoading();
                        }
                    });
        } else
            view.getDataFail(LibraryApplication.getContext().getString(R.string.no_connect_to_internet));
    }

    public void getItem(Listing item, Context activity) {
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra("id", item.getListingID());
        view.moveToDetail(intent);
    }
}