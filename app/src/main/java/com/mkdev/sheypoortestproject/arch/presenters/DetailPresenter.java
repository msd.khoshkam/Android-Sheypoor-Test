package com.mkdev.sheypoortestproject.arch.presenters;

import com.mkdev.mkdevlibrary.LibraryApplication;
import com.mkdev.mkdevlibrary.network.ApiClient;
import com.mkdev.mkdevlibrary.network.RetrofitErrorHelper;
import com.mkdev.mkdevlibrary.utils.NetworkUtil;
import com.mkdev.sheypoortestproject.R;
import com.mkdev.sheypoortestproject.service.ApplicationApi;
import com.mkdev.sheypoortestproject.arch.models.detail.DetailModel;
import com.mkdev.sheypoortestproject.arch.views.activities.detail.DetailView;
import com.mkdev.sheypoortestproject.base.ui.BasePresenter;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DetailPresenter extends BasePresenter<DetailView> {

    public DetailPresenter(DetailView view) {
        super.attachView(view);
    }

    public void loadData(String detailId) {

        if (NetworkUtil.isNetworkConnected(LibraryApplication.getContext())) {
            view.showLoading();

            ApplicationApi mService = ApiClient.getInstance().getService(ApplicationApi.class);
            Observable<DetailModel> mObservable = mService.getDetail(detailId);

            mObservable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<DetailModel>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(DetailModel response) {
                            view.getDataSuccess(response);
                        }

                        @Override
                        public void onError(Throwable e) {
                            String message = (String) new RetrofitErrorHelper().getMessage(e);
                            view.getDataFail(message);
                            view.hideLoading();
                        }

                        @Override
                        public void onComplete() {
                            view.hideLoading();
                        }
                    });
        } else
            view.getDataFail(LibraryApplication.getContext().getString(R.string.no_connect_to_internet));
    }
}