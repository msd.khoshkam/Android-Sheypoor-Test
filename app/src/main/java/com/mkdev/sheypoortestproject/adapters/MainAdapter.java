package com.mkdev.sheypoortestproject.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.mkdev.mkdevlibrary.GlideApp;
import com.mkdev.mkdevlibrary.utils.Check;
import com.mkdev.mkdevlibrary.utils.ConvertDigits;
import com.mkdev.mkdevlibrary.utils.UIUtil;
import com.mkdev.sheypoortestproject.R;
import com.mkdev.sheypoortestproject.arch.models.main.Listing;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Unbinder unbinder;
    private Context mContext;
    private List<Listing> mList;
    private int lastPosition = -1;

    public MainAdapter(List<Listing> list) {
        mList = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        View view = LayoutInflater.from(mContext).inflate(R.layout.row_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Listing item = getItem(position);

        try {
            if (!Check.isEmpty(item.getThumbImageURL())) {
                GlideApp.with(mContext)
                        .load(item.getThumbImageURL())
                        .centerCrop()
                        .placeholder(R.mipmap.img_preview)
                        .error(R.mipmap.img_not_found)
                        .into(viewHolder.imgThumbnail);
            } else
                viewHolder.imgThumbnail.setImageDrawable(UIUtil.getDrawable(mContext, R.mipmap.img_not_found));

            if (!Check.isEmpty(item.getTitle())) {
                viewHolder.tvTitle.setVisibility(View.VISIBLE);
                viewHolder.tvTitle.setText(item.getTitle());
            } else
                viewHolder.tvTitle.setVisibility(View.GONE);

            if (item.getAttributes().size() > 0) {
                viewHolder.tvPrice1.setVisibility(View.VISIBLE);
                if (item.getAttributes().get(0).getAttributeValue().equals("توافقی"))
                    viewHolder.tvPrice1.setText(item.getAttributes().get(0).getAttributeValue());
                else
                    viewHolder.tvPrice1.setText(ConvertDigits.persianDigits(String.format(mContext.getString(R.string.price),
                            item.getAttributes().get(0).getAttributeValue())));
            } else
                viewHolder.tvPrice1.setVisibility(View.GONE);

            if (!Check.isEmpty(item.getLocationName())) {
                viewHolder.tvAddress.setVisibility(View.VISIBLE);
                viewHolder.tvAddress.setText(item.getLocationName());
            } else
                viewHolder.tvAddress.setVisibility(View.GONE);

        } catch (NullPointerException | IndexOutOfBoundsException e) {
            e.printStackTrace();
        }

        //setAnimation(holder.itemView, position);
    }

    /*private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.from_left);
            animation.setDuration(700);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }*/

    @Override
    public int getItemCount() {
        return mList.size();
    }

    private Listing getItem(int position) {
        return mList.get(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener {

        @BindView(R.id.imgThumbnail)
        ImageView imgThumbnail;
        @BindView(R.id.tvTitle)
        TextView tvTitle;
        @BindView(R.id.tvPrice1)
        TextView tvPrice1;
        @BindView(R.id.tvPrice2)
        TextView tvPrice2;
        @BindView(R.id.tvAddress)
        TextView tvAddress;
        @BindView(R.id.cvItem)
        CardView cvItem;

        public ViewHolder(View view) {
            super(view);
            unbinder = ButterKnife.bind(this, view);
            cvItem.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.cvItem:

                    break;
            }
        }
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        if (unbinder != null)
            unbinder.unbind();
    }

    public void update(List<Listing> list) {
        mList = new ArrayList<>();
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void addMoreData(List<Listing> list) {
        mList.addAll(list);
        notifyDataSetChanged();
    }
}
