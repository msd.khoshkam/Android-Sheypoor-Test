package com.mkdev.sheypoortestproject;

import android.content.res.Configuration;

import com.mkdev.mkdevlibrary.LibraryApplication;

import java.util.Locale;

public class MyApplication extends LibraryApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        Locale locale = new Locale("fa");
        Locale.setDefault(locale);
        Configuration mConfiguration = new Configuration();
        mConfiguration.locale = locale;
        getBaseContext().getResources().updateConfiguration(mConfiguration,
                getBaseContext().getResources().getDisplayMetrics());
    }
}
