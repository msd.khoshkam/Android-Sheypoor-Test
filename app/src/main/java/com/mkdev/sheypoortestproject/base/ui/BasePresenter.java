package com.mkdev.sheypoortestproject.base.ui;

public class BasePresenter<V> {
    public V view;

    public void attachView(V view) {
        this.view = view;
    }

    public void detachView() {
        this.view = null;
    }
}