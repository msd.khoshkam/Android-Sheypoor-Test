package com.mkdev.sheypoortestproject.base.ui;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.View;

import com.mkdev.mkdevlibrary.utils.TypeFaceUtils;
import com.mkdev.mkdevlibrary.utils.TypefaceSpan;
import com.mkdev.sheypoortestproject.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class BaseActivity extends AppCompatActivity {

    protected Unbinder unbinder;
    protected List<Call> calls;
    protected Context mContext;
    protected Toolbar toolbar;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initBeforeView();
        setContentView(getContentViewId());
        unbinder = ButterKnife.bind(this);
        mContext = this;
        initViews();
    }

    protected abstract void initBeforeView();

    protected abstract int getContentViewId();

    protected abstract void initViews();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        onCancelled();
    }

    private void onCancelled() {
        if (calls != null && calls.size() > 0) {
            for (Call call : calls) {
                if (!call.isCanceled()) {
                    call.cancel();
                }
            }
        }
    }

    public void addCalls(Call call) {
        if (calls == null) {
            calls = new ArrayList<>();
        }
        calls.add(call);
    }

    protected void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    protected void setupBack(boolean isBack, String title) {
        if (isBack) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        SpannableString s = new SpannableString(title);
        s.setSpan(new TypefaceSpan(mContext, TypeFaceUtils.getFontPath(mContext), 14.0f),
                0, s.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(s);
    }
}
