# Shaypoor test app
This repository contains a test app that implements MVP architecture using RxJava2, Retrofit2, Glide, Calligraphy and Butterknife.

# Preview
![img 1](http://s9.picofile.com/file/8319995934/Screenshot_20180221_161053.png) 
![img 1](http://s8.picofile.com/file/8319995942/Screenshot_20180221_161108.png) 
![img 2](http://s9.picofile.com/file/8319995950/Screenshot_20180221_161113.png) 
![img 2](http://s9.picofile.com/file/8319995968/Screenshot_20180221_161119.png) 

This app will show a list of items with a view for details of each item that implement from 2 view for app, main page and detail page.

**The app has following packages:**
1. **base:** It's included base class of activity, fragment and mvp binding classes.
   - mvp
   - ui
2. **arch:** It's included MVP layers.
   - models
   - views
   - presenters
3. **service:** Services for the application.
4. **adapters:** 

and import a modules contains utils and base class and styles.

Classes have been designed in such a way that it could be inherited and maximize the code reuse.

**Main Activity**

Contains a list of all items with this feature : 
1. The page has infinite scroll
2. Each Item has (single image/title/price) and it has been handled some items may have no price
3. Each item has a detail page
4. Contains sort items by price (asc,desc)
5. Contains filter to see the items which has image

**Detail Activity**

1. The page has all information about single item
2. The page has a single image box on top with collapsing layout

