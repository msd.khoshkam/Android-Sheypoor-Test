package com.mkdev.mkdevlibrary;

import android.app.Application;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.multidex.MultiDex;
import com.bumptech.glide.Glide;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class LibraryApplication extends Application {

    private static Application sApplication;
    private static boolean activityVisible;

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Glide.get(this).clearMemory();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sApplication = this;
        setupCalligraphy();
    }

    private void setupCalligraphy() {

        int[] attrs = {R.attr.fontPath};
        TypedArray a = obtainStyledAttributes(R.style.StandardText, attrs);
        String fontPath = a.getString(0);
        a.recycle();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath(fontPath)
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static synchronized Application getContext() {
        return sApplication;
    }

    public static boolean isActivityVisible() {
        return activityVisible;
    }

    public static void activityResumed() {
        activityVisible = true;
    }

    public static void activityPaused() {
        activityVisible = false;
    }
}
