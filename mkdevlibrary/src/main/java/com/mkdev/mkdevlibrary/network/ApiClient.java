package com.mkdev.mkdevlibrary.network;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.mkdev.mkdevlibrary.LibraryApplication;
import com.mkdev.mkdevlibrary.MyLibraryConstant;
import com.mkdev.mkdevlibrary.utils.Check;
import com.mkdev.mkdevlibrary.utils.FileUtil;
import com.mkdev.mkdevlibrary.utils.NetworkUtil;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static final int HTTP_RESPONSE_DISK_CACHE_MAX_SIZE = 10 * 1024 * 1024;
    private static final int MAX_AGE = 60 * 10; //with network 10min
    private static final int MAX_STALE = 60 * 60 * 24; //1 day ,no network

    private static final String WEB_SERVICE_BASE_URL = "https://www.sheypoor.com/api/v3/";

    private volatile static ApiClient sAppClient;
    private Map<String, Object> serviceByType = new HashMap<>();
    private Retrofit mRetrofit;

    private ApiClient() {

        OkHttpClient client = new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .addInterceptor(cacheInterceptor())
                .cache(cache())
                .build();

        mRetrofit = new Retrofit
                .Builder()
                .baseUrl(WEB_SERVICE_BASE_URL)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    private Cache cache() {
        final File baseDir = FileUtil.getExternalCacheDir(LibraryApplication.getContext());
        final File cacheDir = new File(baseDir, "HttpResponseCache");
        return (new Cache(cacheDir, HTTP_RESPONSE_DISK_CACHE_MAX_SIZE));

    }

    private Interceptor cacheInterceptor() {

        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {

                Request original = chain.request();
                if (!NetworkUtil.isNetworkConnected(LibraryApplication.getContext())) {
                    original = original.newBuilder().cacheControl(CacheControl.FORCE_CACHE).build();
                }
                Request.Builder builder = original.newBuilder();
                Request request = builder.method(original.method(), original.body()).build();

                return chain.proceed(request);
            }
        };
    }

    public static ApiClient getInstance() {
        synchronized (ApiClient.class) {
            if (sAppClient == null) {
                sAppClient = new ApiClient();
            }
        }
        return sAppClient;
    }

    public synchronized <T> T getService(Class<T> apiInterface) {
        String serviceName = apiInterface.getName();
        if (!Check.isNull(serviceByType.get(serviceName))) {
            return (T) serviceByType.get(serviceName);
        }
        T service = mRetrofit.create(apiInterface);
        serviceByType.put(serviceName, service);
        return service;
    }

}
