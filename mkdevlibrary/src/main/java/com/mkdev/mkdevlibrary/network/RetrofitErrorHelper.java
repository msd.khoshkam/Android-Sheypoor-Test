package com.mkdev.mkdevlibrary.network;

import android.accounts.NetworkErrorException;
import android.util.Log;
import com.google.gson.Gson;
import com.mkdev.mkdevlibrary.LibraryApplication;
import com.mkdev.mkdevlibrary.R;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import retrofit2.Response;

public class RetrofitErrorHelper {

    private ApiErrorModel errorResponse = new ApiErrorModel();

    public Object getMessage(Object error) {

        Log.d("R_Error", String.valueOf(error));

        if (String.valueOf(error).equals(LibraryApplication.getContext().getString(R.string.no_connect_to_internet))) {
            errorResponse.setMessage(LibraryApplication.getContext().getString(R.string.no_connect_to_internet));
            return errorResponse;
        } else if (error instanceof SocketTimeoutException) {
            errorResponse.setMessage(LibraryApplication.getContext().getString(R.string.SERVER_TIMEOUT));
            return errorResponse;
        } else if (isNetworkProblem(error)) {
            errorResponse.setMessage(LibraryApplication.getContext().getString(R.string.no_connect_to_server));
            return errorResponse;
        } else {
            return handleServerError(error);
        }
    }

    private static boolean isNetworkProblem(Object error) {
        return (error instanceof ConnectException) || (error instanceof NetworkErrorException);
    }

    private Object handleServerError(Object error) {
        Response response;
        try {
            response = (Response) error;
            Log.d("R_Error_Code", String.valueOf(error));
        } catch (ClassCastException e) {
            e.printStackTrace();
            return LibraryApplication.getContext().getString(R.string.SERVER_ERROR);
        }

        try {

            Gson gson = new Gson();
            errorResponse = gson.fromJson(
                    response.errorBody().string(),
                    ApiErrorModel.class);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        switch (response.code()) {
            case 400: // not found
                return errorResponse;
            case 404:
                errorResponse.setMessage(LibraryApplication.getContext().getString(R.string.ERROR_PAGE_NOT_FOUND));
                return errorResponse;
            case 405: // server error
            case 500:
                errorResponse.setMessage(LibraryApplication.getContext().getString(R.string.no_connect_to_server));
                return errorResponse;

            case 422: // validation

            case 401: // autorization - refresh token
                return errorResponse;

            default:
                errorResponse.setMessage(LibraryApplication.getContext().getString(R.string.no_connect_to_server));
                return errorResponse;
        }
    }
}
