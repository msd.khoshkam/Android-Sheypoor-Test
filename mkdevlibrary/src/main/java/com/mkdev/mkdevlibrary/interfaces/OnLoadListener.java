package com.mkdev.mkdevlibrary.interfaces;

public interface OnLoadListener<A> {
    void onLoadCallback(A response);
}
