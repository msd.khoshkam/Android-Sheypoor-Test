package com.mkdev.mkdevlibrary.interfaces;

public interface MyCallback<A, B> {
    void onSuccessful(A response);

    void onFailure(B response);
}
