package com.mkdev.mkdevlibrary.interfaces;

public interface OnClickListener<A> {
    void onClickCallback(Integer requestCode, A response);
}
