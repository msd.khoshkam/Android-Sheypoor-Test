package com.mkdev.mkdevlibrary.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.LruCache;

import com.mkdev.mkdevlibrary.R;

public class TypeFaceUtils {

    /**
     * @return IRANSansMobileRegular.ttf
     */
    public static String getFontName(Context context) {
        int[] attrs = {R.attr.fontPath};
        TypedArray a = context.obtainStyledAttributes(R.style.StandardText, attrs);
        return a.getString(0).replace("fonts/", "");
    }

    /**
     * @return fonts/IRANSansMobileRegular.ttf
     */
    public static String getFontPath(Context context) {
        int[] attrs = {R.attr.fontPath};
        TypedArray a = context.obtainStyledAttributes(R.style.StandardText, attrs);
        return a.getString(0);
    }

    /**
     * retuen Typeface file
     * @param context
     * @return
     */
    public static Typeface getTypeFace(Context context) {

        LruCache<String, Typeface> sTypeFaceCache = new LruCache<>(12);
        Typeface mTypeFace = sTypeFaceCache.get(getFontName(context));

        if (mTypeFace == null) {
            mTypeFace = Typeface.createFromAsset(context.getAssets(), getFontPath(context));
            sTypeFaceCache.put(getFontName(context), mTypeFace);
        }
        return mTypeFace;
    }

}
