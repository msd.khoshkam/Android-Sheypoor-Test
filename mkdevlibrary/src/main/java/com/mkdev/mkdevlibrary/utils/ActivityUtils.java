package com.mkdev.mkdevlibrary.utils;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.mkdev.mkdevlibrary.R;

public final class ActivityUtils {

    public static void addFragmentToActivity(FragmentManager fragmentManager,
                                             Fragment fragment,
                                             int frameId) {

        Fragment oldFragment = fragmentManager.findFragmentById(frameId);
        if (oldFragment != null) {
            fragmentManager.beginTransaction().remove(oldFragment).commit();
        }

        if (!fragment.isAdded()) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(frameId, fragment);
            transaction.commit();
        }
    }

    public static void replaceFragmentToActivity(FragmentManager fragmentManager,
                                                 Fragment fragment,
                                                 int frameId,
                                                 String TARGET_FRAGMENT_TAG,
                                                 String SOURCE_FRAGMENT_TAG) {

        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        transaction.replace(frameId, fragment, TARGET_FRAGMENT_TAG);
        transaction.addToBackStack(SOURCE_FRAGMENT_TAG);
        transaction.commit();
    }

    public static void replaceFragmentToActivity(FragmentManager fragmentManager,
                                                 Fragment fragment,
                                                 int frameId) {

        final FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
        transaction.replace(frameId, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public static void restartActivity(Activity activity) {
        Intent intent = activity.getIntent();
        activity.finish();
        activity.startActivity(intent);
    }

    public static void showDialogFragment(FragmentManager fragmentManager,
                                          String FRAGMENT_TAG,
                                          DialogFragment newFragment) {

        FragmentTransaction ft = fragmentManager.beginTransaction();
        Fragment prev = fragmentManager.findFragmentByTag(FRAGMENT_TAG);
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        // Create and show the dialog.
        newFragment.show(ft, FRAGMENT_TAG);
    }
}
